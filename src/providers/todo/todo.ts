import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AlertController } from 'ionic-angular';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

export interface Todo { title: string; }
export interface TodoId { id: string }

@Injectable()
export class TodoProvider {

    private todos: Observable<TodoId[]>;
    private archivedTodos: Observable<TodoId[]>;

    private todosCollection: AngularFirestoreCollection<Todo>;
    private archivedCollection: AngularFirestoreCollection<Todo>;

    constructor(
        public http: HttpClient,
        private alertController: AlertController,
        private afs: AngularFirestore) {

        //gets collections
        this.todosCollection = this.afs.collection('todos');
        this.archivedCollection = this.afs.collection('archives');

        //gets collection data
        //this.todos = this.todosCollection.valueChanges();
        //this.archivedTodos = this.archivedCollection.valueChanges();

        //getting more structured data set
        this.todos = this.todosCollection.snapshotChanges().map(action => {
            return action.map(a => {
                const data = a.payload.doc.data() as Todo;
                const id = a.payload.doc.id;
                return { id, ...data };
            });
        });

        this.archivedTodos = this.archivedCollection.snapshotChanges().map(action => {
            return action.map(a => {
                const data = a.payload.doc.data() as Todo;
                const id = a.payload.doc.id;
                return { id, ...data };
            });
        });

    }

    getTodos() {
        return this.todos;
    }

    getArchived() {
        return this.archivedTodos;
    }

    addTodo(title) {

        // Persist a document id
        const id = this.afs.createId();
        const item = { title };

        this.todosCollection.doc(id).set(item);
    }

    addArchive(todo) {
        //set stand alone object without the ID
        const item = {title: todo.title};
        this.archivedCollection.doc(todo.id).set(item);

        this.todosCollection.doc(todo.id).delete();
    }

    delete(todo) {
        let addDeleteAlert = this.alertController.create({
            title: 'Delete Todo',
            subTitle: "You can't undo this action",
            buttons: [
            {
                text: 'Cancel'
            },
            {
                text: 'Delete',
                handler: () => {
                    this.todosCollection.doc(todo.id).delete();
                }
            }
            ]
        });

        addDeleteAlert.present();
    }

    deleteArchive(todo) {
        let addDeleteAlert = this.alertController.create({
            title: 'Delete Archive',
            subTitle: "You can't undo this action",
            buttons: [
            {
                text: 'Cancel'
            },
            {
                text: 'Delete',
                handler: () => {
                    this.archivedCollection.doc(todo.id).delete();
                }
            }
            ]
        });

        addDeleteAlert.present();
    }

    editTodo(todo) {
        let editAlert = this.alertController.create({
            title: 'Edit Todo',
            inputs: [
            {
                name: 'title',
                value: todo.title
            }
            ],
            buttons: [
            {
                text: 'Cancel'
            },
            {
                text: 'Edit',
                handler: (input) => {
                    this.todosCollection.doc(todo.id).update(input);
                }
            }
            ]
        });

        editAlert.present();
    }
}
