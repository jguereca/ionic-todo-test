import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ArchivedTodosPage } from '../pages/archived-todos/archived-todos'

import { TodoProvider } from '../providers/todo/todo';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
    apiKey: "AIzaSyBImzsQoG4XlE9d42dDjTPO4vS1uWv_qpM",
    authDomain: "todo-test-32741.firebaseapp.com",
    databaseURL: "https://todo-test-32741.firebaseio.com",
    projectId: "todo-test-32741",
    storageBucket: "todo-test-32741.appspot.com",
    messagingSenderId: "884865177362"
};

@NgModule({
    declarations: [
    MyApp,
    HomePage,
    ArchivedTodosPage
    ],
    imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
    MyApp,
    HomePage,
    ArchivedTodosPage
    ],
    providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TodoProvider,
    AngularFirestore
    ]
})
export class AppModule {}
