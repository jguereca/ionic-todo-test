import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { TodoProvider } from '../../providers/todo/todo';

@Component({
    selector: 'page-archived-todos',
    templateUrl: 'archived-todos.html',
})
export class ArchivedTodosPage {

    archives: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private todoProvider: TodoProvider) {

        this.archives = this.todoProvider.getArchived();
    }

    deleteTodo(itemIndex) {
        this.todoProvider.deleteArchive(itemIndex);
    }

}
