import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { TodoProvider } from '../../providers/todo/todo';
import { ArchivedTodosPage } from '../archived-todos/archived-todos';

import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    title: string;
    todos: any;
    reorderEnabled: boolean;
    user: Observable<firebase.User>;
    displayName: string;

    constructor(
        public navCtrl: NavController,
        private alertController: AlertController,
        private toastController: ToastController,
        private fireAuth: AngularFireAuth,
        private todoProvider: TodoProvider) {

        this.title = 'What Are We Todo?';
        this.todos = this.todoProvider.getTodos();

        this.reorderEnabled = false;

        fireAuth.authState.subscribe(user => {

            if (!user) {
                this.displayName = 'Not Logged in';
                return; //exists subscribe
            }
            this.displayName = user.displayName;
        });
    }

    signIn() {
        this.fireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(res => {

            let loginToast = this.toastController.create({
                message: 'Successful Login',
                duration: 2000
            });

            this.user = res.user;

            loginToast.present();
        });
    }

    openTodoAlert() {
        let addTodoAlert = this.alertController.create({
            title: 'Add Todo',
            message: 'Enter your todo',
            inputs: [
            {
                type: 'text',
                name: 'todo'
            }
            ],
            buttons: [
            {
                text: 'Cancel'
            },
            {
                text: 'Add',
                handler: data => {
                    this.todoProvider.addTodo(data.todo);

                    addTodoAlert.onDidDismiss(() => {
                        let successToast = this.toastController.create({
                            message: "Todo Added Successfully",
                            duration: 2000
                        });

                        successToast.present();
                    });
                }
            }
            ]
        });

        addTodoAlert.present();
    }

    editTodo(todo) {
        this.todoProvider.editTodo(todo);
    }

    toggleReorder() {
        this.reorderEnabled = !this.reorderEnabled;
    }

    //CURRENTLY NOT FUNCTIONING
    itemsReordered(event) {
        console.log(event);
        //reorderArray(this.todos, event);
    }

    deleteTodo(todo) {
        this.todoProvider.delete(todo);
    }

    archiveTodo(todo) {
        this.todoProvider.addArchive(todo);
    }

    goToArchivePage() {
        this.navCtrl.push(ArchivedTodosPage);
    }

}
